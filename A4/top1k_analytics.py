from pyspark import SparkContext
from subprocess import Popen,PIPE
sc = SparkContext.getOrCreate()

top1m = sc.textFile("/top-1m.csv").cache()
top1k = top1m.take(1000)

def google_analytics(url):
    
    try:
        page =  Popen(['curl','-s','-L',url],stdout=PIPE).communicate()[0].decode('utf-8','ignore') 
    except:
        print ("The web page is not in ASCII or UTF-8")
    return (("analytics.js" in page) or ("ga.js" in page))
    

result = open ("top1k_analytics.txt","w")
for line in top1k:
    rank_domain = line.split(",")
    print (rank_domain)
    if google_analytics(rank_domain[1].strip()) :
        result.write('%s, %s\n' % (rank_domain[1].strip(), "True"))
result.close()




