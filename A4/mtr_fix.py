#!/usr/bin/env python35
#
# Parse an MTR file in text mode and turn it into a tinydata file.
#
import sys
if sys.version < "3.4":
    raise RuntimeError("requires Pyton 3.4 or above")

import os,datetime,re,time
mtr_line_exp = re.compile("")   # put something here
IP_RE = re.compile(r"((?:(?:25[0-5]|2[0-4]\d|((1\d{2})|([1-9]?\d)))\.){3}(?:25[0-5]|2[0-4]\d|((1\d{2})|([1-9]?\d))))")
HOST_RE = re.compile(r"((\w+)+(\-\w+){1,10}(\.\w+){1,10})")
DICT_RE = re.compile(r"^((\S+)\;(\S+)\;(\S+)\;(\S+)\;(\S+))\;(\S+) (\S+)\;(\S+)")


def parse_timestamp(line):
   assert line.startswith("Start:")
   temp_date = line.lstrip("Start:").strip()

   dateArray = time.strptime(temp_date, "%a %b %d %H:%M:%S %Y")
   return datetime.datetime.fromtimestamp(time.mktime(dateArray)).isoformat()
   # return "Return an ISO8601-formatted timestamp from an input line")



class MtrLine:
   def __init__(self,ts,line):
       words = line.split(" ")
       while "" in words:
           words.remove("")
       self.timeStamp = ts
       self.hop_number = int(words[0].rstrip(".|--"))
       if words[2] == "0.0%":
           self.pct_loss = "0"
       elif words[2] == "100.0":
           self.pct_loss = "100"
       self.pct_loss = int(self.pct_loss)
       self.last_time = float(words[4])
       ip_re_result = IP_RE.search(words[1])
       host_re_result = HOST_RE.search(words[1])
       if ip_re_result:
           self.ipaddr = ip_re_result.group(0)
           key = [key for key, value in ip_host_dict.items() if value.find(self.ipaddr) != -1]
           if key:
               self.hostname = key[0]
           else:
               self.hostname = ""
       elif host_re_result:
           temp_host = host_re_result.group(0)
           key = [key for key, value in ip_host_dict.items() if key.find(temp_host) != -1]
           if key:
               self.hostname = key[0]
               self.ipaddr = ip_host_dict[self.hostname]
           else:
               self.hostname = temp_host
               self.ipaddr = ""
       else:
           self.ipaddr = "???"
           self.hostname = "???"

        # Parse line and fill these in.
        # ts is the timestamp that we previously found


def creat_dict():
    ip_host_dict = dict()
    with open('mtr.www.comcast.com.2016.txt','r') as f:
        for line in f:
            m = DICT_RE.search(line)
            if m:
                host = m.group(7)
                ip = m.group(8).lstrip("(").rstrip(")")
                ip_host_dict [host] = ip
    return ip_host_dict

def fix_mtr(infile,outfile):
    for line in infile:
        line = line.strip()     # remove leading and trailing white space
        line = line.replace('\x00', '')
        if line.startswith('Start:'):
            ts = parse_timestamp(line)

            # Beginning of a new record...
            # print("Replace this print statement with new code. Probably need to set a variable with the time...")
            continue

        elif line.startswith('HOST:'):
            # This can be ignored, since we always start at the same location
            continue

        else:
            m = MtrLine(ts,line)
            outfile.write("%s, %d, %s, %s, %d, %.1f\n"%(m.timeStamp,m.hop_number,m.ipaddr,m.hostname,m.pct_loss,m.last_time))


if __name__=="__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--infile",help="input file",default="mtr.www.cnn.com.txt")
    parser.add_argument("--outfile",help="output file",default="mtr_fix.txt")
    args = parser.parse_args()
    if os.path.exists(args.outfile):
        raise RuntimeError(args.outfile + " already exists. Please delete it.")

    print("{} -> {}".format(args.infile,args.outfile))
    ip_host_dict = creat_dict()
    fix_mtr(open(args.infile,"rU"), open(args.outfile,"w"))

