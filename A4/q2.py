from pyspark import SparkContext
import re

sc = SparkContext.getOrCreate()

top1m = sc.textFile("/top-1m.csv").cache()

com_pattern = re.compile(r".com$")

com_result = top1m.filter(lambda line:  com_pattern.search(line))

result = open ("q2.txt","w")
result.write(str(com_result.count()))
result.close()
