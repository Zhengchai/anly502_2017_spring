JFK Airport	721361.75
East Village	718072.9000000001
Lower East Side	628319.73
Times Sq/Theatre District	507464.91000000003
Clinton East	497585.23
West Village	489181.92000000004
East Chelsea	429749.9
Midtown South	379483.29
Meatpacking/West Village West	374220.98
Union Sq	368466.01999999996
Greenwich Village South	361617.84
LaGuardia Airport	345021.42000000004
Midtown North	308815.25
Midtown Center	297086.66
TriBeCa/Civic Center	292465.54
Penn Station/Madison Sq West	288325.64
Murray Hill	266868.8
Gramercy	260476.49999999994
Midtown East	244363.49
Flatiron	241227.94999999998
Little Italy/NoLiTa	237101.80000000005
Garment District	223094.93
Sutton Place/Turtle Bay North	194305.64999999997
Greenwich Village North	158055.47999999998
Lenox Hill West	150874.51
West Chelsea/Hudson Yards	147139.68
Lincoln Square East	146411.28000000003
SoHo	121084.3
Upper East Side South	120972.37999999999
Clinton West	118588.43000000001
Upper West Side South	115502.37000000001
Yorkville West	111699.89000000001
Kips Bay	102208.20000000001
Financial District North	100818.30000000002
UN/Turtle Bay South	88559.87000000001
Upper West Side North	73682.6
Hudson Sq	70867.39
Financial District South	67047.8
Upper East Side North	66960.38
Williamsburg (North Side)	62912.21
World Trade Center	61851.200000000004
Lenox Hill East	60143.72
Alphabet City	57619.9
Williamsburg (South Side)	46778.7
Battery Park City	46561.86000000001
Two Bridges/Seward Park	45696.61
Astoria	39652.310000000005
Morningside Heights	37197.33
Central Park	35358.51
East Harlem South	35295.130000000005
Manhattan Valley	35204.479999999996
Chinatown	35152.2
Park Slope	34812.149999999994
East Harlem North	33706.25
Lincoln Square West	33641.35
Central Harlem	32612.819999999996
Long Island City/Hunters Point	30344.29
East Williamsburg	29809.199999999997
Seaport	29695.32
Yorkville East	28094.5
Sunnyside	26353.46
Boerum Hill	24260.6
Fort Greene	22870.72
Downtown Brooklyn/MetroTech	22110.23
Bloomingdale	21803.3
Greenpoint	21516.84
Long Island City/Queens Plaza	19055.83
Bushwick South	17962.93
Central Harlem North	17563.399999999998
Hamilton Heights	16557.4
Stuy Town/Peter Cooper Village	16537.11
Clinton Hill	12771.439999999999
Brooklyn Heights	12600.94
Bedford	12474.8
Jackson Heights	12393.84
Washington Heights South	11414.74
Crown Heights North	10516.62
Manhattanville	9851.6
Steinway	9809.199999999999
Prospect Heights	9389.310000000001
Bushwick North	9356.6
Woodside	8395.220000000001
Old Astoria	8248.8
Carroll Gardens	8238.78
Elmhurst	7665.3
Gowanus	7235.8
Stuyvesant Heights	6201.400000000001
Washington Heights North	5075.800000000001
DUMBO/Vinegar Hill	5019.38
Flatbush/Ditmas Park	4901.610000000001
Queensbridge/Ravenswood	4518.9
Prospect-Lefferts Gardens	4238.610000000001
Cobble Hill	4014.5
Baisley Park	3725.3
Sunset Park West	3610.1800000000003
Mott Haven/Port Morris	3289.62
Elmhurst/Maspeth	3257.02
Forest Hills	3181.3
Flushing Meadows-Corona Park	3057.5
Battery Park	2975.6
South Williamsburg	2881.51
West Concourse	2772.0
Crown Heights South	2174.9
Rego Park	2121.3
Newark Airport	2116.2799999999997
Inwood	2107.8
East Elmhurst	2001.0
Bay Ridge	1743.1
Flushing	1742.0
Ridgewood	1722.1
Kensington	1604.0
Jamaica	1551.85
Springfield Gardens South	1514.8
South Jamaica	1317.0
Maspeth	1310.0
South Ozone Park	1307.54
Windsor Terrace	1236.8
East Concourse/Concourse Village	1220.3
Briarwood/Jamaica Hills	1213.51
Prospect Park	1183.8
Brownsville	1076.0
Mount Hope	1043.6
Melrose South	1038.0
Roosevelt Island	1025.3
Columbia Street	994.1
Corona	957.5
Erasmus	953.5
Bensonhurst West	938.6800000000001
East New York	914.0
Kew Gardens	909.76
Richmond Hill	894.3
Ocean Hill	858.5
Randalls Island	840.5
North Corona	824.0
East Flatbush/Remsen Village	776.0
Canarsie	763.8
Borough Park	728.5
Red Hook	716.0
Highbridge	697.0
Morrisania/Melrose	664.8
East Flatbush/Farragut	620.35
Queensboro Hill	576.5
Sunset Park East	569.0
Woodlawn/Wakefield	568.51
Norwood	567.1
Glen Oaks	550.5
University Heights/Morris Heights	545.31
Bensonhurst East	535.5
Gravesend	507.0
Bedford Park	491.04999999999995
East New York/Pennsylvania Avenue	473.01
Crotona Park East	462.5
Woodhaven	460.34000000000003
Fresh Meadows	453.5
East Tremont	450.0
Flatlands	443.5
Brooklyn Navy Yard	430.0
Bronx Park	421.0
Stapleton	412.0
Belmont	408.5
Homecrest	398.5
Midwood	397.5
Middle Village	392.0
West Farms/Bronx River	386.0
Dyker Heights	356.5
Riverdale/North Riverdale/Fieldston	356.34000000000003
Sheepshead Bay	338.5
Coney Island	323.5
Hillcrest/Pomonok	320.0
Hunts Point	318.5
Kew Gardens Hills	316.0
Jamaica Estates	316.0
Glendale	307.0
Ozone Park	302.0
Soundview/Bruckner	300.5
Howard Beach	298.2
Bayside	287.0
Soundview/Castle Hill	286.15
Kingsbridge Heights	283.0
Van Cortlandt Village	282.5
Queens Village	262.0
Longwood	259.0
Cypress Hills	252.0
Allerton/Pelham Gardens	251.0
College Point	232.90999999999997
Claremont/Bathgate	231.0
Arrochar/Fort Wadsworth	228.43
Spuyten Duyvil/Kingsbridge	228.0
Parkchester	227.2
Bath Beach	218.0
Fordham South	215.5
Westchester Village/Unionport	214.0
Springfield Gardens North	211.5
West Brighton	207.0
Saint Albans	205.5
South Beach/Dongan Hills	202.0
Highbridge Park	195.01
Pelham Parkway	194.06
Williamsbridge/Olinville	191.5
Rockaway Park	189.5
Van Nest/Morris Park	187.5
Rosedale	179.0
Co-Op City	174.0
Astoria Park	169.5
Heartland Village/Todt Hill	167.7
Murray Hill-Queens	166.5
Great Kills	160.0
Manhattan Beach	148.5
Marble Hill	135.5
Marine Park/Mill Basin	135.5
Bronxdale	133.5
Brighton Beach	125.0
Douglaston	115.0
Saint George/New Brighton	113.5
Whitestone	113.0
Eltingville/Annadale/Prince's Bay	109.5
Oakwood	106.85
Madison	106.0
Bellerose	104.5
Oakland Gardens	99.5
Schuylerville/Edgewater Park	98.0
New Dorp/Midland Beach	92.5
Ocean Parkway South	88.0
Jamaica Bay	87.0
Rossville/Woodrow	85.38
Cambria Heights	81.0
Grymes Hill/Clifton	76.0
East Flushing	76.0
Starrett City	72.5
Bloomfield/Emerson Hill	70.0
Van Cortlandt Park	69.5
Willets Point	65.5
Eastchester	60.0
Hollis	55.5
Marine Park/Floyd Bennett Field	54.5
Pelham Bay	51.5
Laurelton	50.5
Governor's Island/Ellis Island/Liberty Island	41.5
Inwood Hill Park	35.5
Hammels/Arverne	31.0
Auburndale	24.0
Saint Michaels Cemetery/Woodside	21.0
Green-Wood Cemetery	19.5
Freshkills Park	18.5
Pelham Bay Park	14.5
Country Club	12.5
Forest Park/Highland Park	10.5
Bay Terrace/Fort Totten	10.0
Crotona Park	10.0
Port Richmond	8.0
Far Rockaway	0.0
