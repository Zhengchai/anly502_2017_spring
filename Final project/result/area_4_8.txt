Penn Station/Madison Sq West	43844
Clinton East	39388
Upper East Side North	30970
Murray Hill	27656
Garment District	27329
Upper East Side South	27138
Lenox Hill West	26533
East Village	24647
Gramercy	24432
Midtown East	23100
Yorkville West	21490
JFK Airport	20895
East Chelsea	20619
Times Sq/Theatre District	20444
Yorkville East	19846
Lenox Hill East	18083
Lincoln Square East	18042
Upper West Side South	17980
Upper West Side North	17726
Sutton Place/Turtle Bay North	17615
West Village	17110
Union Sq	16963
Midtown Center	16882
Midtown South	16789
Flatiron	16510
TriBeCa/Civic Center	14563
Midtown North	13908
Kips Bay	13813
UN/Turtle Bay South	12941
Greenwich Village North	12146
Lincoln Square West	11250
Lower East Side	10056
Battery Park City	9320
Clinton West	9240
East Harlem South	9221
West Chelsea/Hudson Yards	9116
Manhattan Valley	8740
Meatpacking/West Village West	8514
Financial District North	7589
Central Park	7281
Greenwich Village South	6887
East Harlem North	5723
Stuy Town/Peter Cooper Village	5466
LaGuardia Airport	5266
Central Harlem	5139
Little Italy/NoLiTa	4598
Morningside Heights	4488
Sunnyside	4405
Astoria	3905
Hudson Sq	3599
SoHo	3524
Bloomingdale	3375
Central Harlem North	3363
Alphabet City	3058
World Trade Center	3033
Brooklyn Heights	2562
Financial District South	2188
Woodside	2126
Steinway	1933
Hamilton Heights	1925
Downtown Brooklyn/MetroTech	1878
Washington Heights South	1767
Jackson Heights	1700
Boerum Hill	1676
Old Astoria	1672
Two Bridges/Seward Park	1536
Long Island City/Queens Plaza	1535
Cobble Hill	1448
Carroll Gardens	1381
Park Slope	1272
Manhattanville	1234
Seaport	1201
Queensbridge/Ravenswood	1164
Chinatown	1150
Williamsburg (South Side)	1070
Long Island City/Hunters Point	1026
Fort Greene	1011
Williamsburg (North Side)	773
Elmhurst	747
East Williamsburg	645
DUMBO/Vinegar Hill	614
Greenpoint	554
Forest Hills	479
Clinton Hill	452
Bushwick South	450
Elmhurst/Maspeth	367
Crown Heights North	336
Bedford	310
Mott Haven/Port Morris	305
West Concourse	295
East Elmhurst	295
Prospect-Lefferts Gardens	271
Washington Heights North	223
Sunset Park West	222
Prospect Heights	211
Stuyvesant Heights	209
Flatbush/Ditmas Park	202
Rego Park	193
Saint Michaels Cemetery/Woodside	175
Newark Airport	172
Baisley Park	168
Bushwick North	168
Columbia Street	160
Gowanus	152
Kensington	145
Inwood	133
Flushing Meadows-Corona Park	122
Melrose South	114
Briarwood/Jamaica Hills	110
Battery Park	107
Jamaica	103
South Jamaica	90
Windsor Terrace	87
Kew Gardens	81
East Concourse/Concourse Village	79
Morrisania/Melrose	73
South Ozone Park	71
Highbridge	62
Red Hook	62
Prospect Park	60
Corona	59
Flushing	58
Crown Heights South	56
Ridgewood	53
North Corona	52
Erasmus	51
Randalls Island	49
Flatlands	48
Springfield Gardens South	47
South Williamsburg	46
Maspeth	44
Bay Ridge	43
Richmond Hill	41
Borough Park	39
Roosevelt Island	38
Woodhaven	38
East New York	38
Ocean Hill	36
Ocean Parkway South	34
University Heights/Morris Heights	33
Grymes Hill/Clifton	29
Kew Gardens Hills	28
Arrochar/Fort Wadsworth	28
Westchester Village/Unionport	28
Claremont/Bathgate	27
Kingsbridge Heights	27
Van Cortlandt Village	26
Midwood	26
Astoria Park	23
Sunset Park East	21
Brownsville	21
Homecrest	20
Canarsie	19
Soundview/Bruckner	19
Mount Hope	18
Cypress Hills	18
Glendale	17
Brighton Beach	16
Norwood	15
Fordham South	15
East Flatbush/Farragut	15
Oakland Gardens	14
Hunts Point	14
Parkchester	14
Bensonhurst East	14
Howard Beach	14
Longwood	13
East Flatbush/Remsen Village	13
Middle Village	13
Crotona Park East	13
Van Nest/Morris Park	13
Soundview/Castle Hill	13
Williamsbridge/Olinville	12
Bensonhurst West	11
Dyker Heights	11
Bedford Park	11
Hillcrest/Pomonok	11
Spuyten Duyvil/Kingsbridge	11
Murray Hill-Queens	11
Queensboro Hill	11
Pelham Parkway	10
Springfield Gardens North	10
Saint George/New Brighton	10
Ozone Park	10
Gravesend	10
Brooklyn Navy Yard	10
Marble Hill	9
Riverdale/North Riverdale/Fieldston	9
Jamaica Estates	9
Inwood Hill Park	9
Stapleton	9
College Point	8
Queens Village	8
East New York/Pennsylvania Avenue	8
Allerton/Pelham Gardens	8
East Tremont	8
Bath Beach	7
Bellerose	7
West Farms/Bronx River	7
Schuylerville/Edgewater Park	6
Bayside	5
Glen Oaks	5
Marine Park/Mill Basin	5
Co-Op City	5
East Flushing	5
Forest Park/Highland Park	5
Highbridge Park	5
Whitestone	4
Sheepshead Bay	4
Coney Island	4
Madison	4
Bronxdale	4
Belmont	4
Manhattan Beach	3
Cambria Heights	3
Fresh Meadows	3
Bronx Park	3
Rockaway Park	3
Woodlawn/Wakefield	3
Governor's Island/Ellis Island/Liberty Island	2
Westerleigh	2
Starrett City	2
Saint Albans	2
Laurelton	2
Marine Park/Floyd Bennett Field	2
Pelham Bay	2
Bay Terrace/Fort Totten	2
Rosedale	2
Douglaston	1
Mariners Harbor	1
City Island	1
Port Richmond	1
Auburndale	1
Eltingville/Annadale/Prince's Bay	1
Far Rockaway	1
Green-Wood Cemetery	1
Crotona Park	1
South Beach/Dongan Hills	1
Bloomfield/Emerson Hill	1
