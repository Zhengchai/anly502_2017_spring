import heapq

from mrjob.job import MRJob
from mrjob.step import MRStep
import re

WORD_RE= re.compile(r"(/[^\s]*)")
TOPN=10
class WordCountTopN(MRJob):
    def mapper(self, _, line):
        word=WORD_RE.findall(line)[1]
        yield word.lower(), 1
    def reducer(self, word, counts):
        yield word, sum(counts)

    def topN_mapper(self, word, count):
        yield "Top" + str(TOPN), (count, word)
    def topN_reducer(self, _, countsAndwords):
        for countsAndwords in heapq.nlargest(TOPN, countsAndwords):
            yield countsAndwords

    def steps(self):
        return [
            MRStep(mapper=self.mapper,reducer=self.reducer),
            MRStep(mapper=self.topN_mapper,reducer=self.topN_reducer)
        ]

if __name__=="__main__":
    WordCountTopN.run()
