#!/usr/lib/env python35

# for more information about matplotlib, see:
# http://matplotlib.org/users/pyplot_tutorial.html

import numpy as np
import matplotlib.pyplot as plt


def plot(results):
    png_fname = "q3_plot.png"
    pdf_fname = "q3_plot.pdf"
    import matplotlib.pyplot as plt
    import ast

    xpoints = [2 ,5 ,10 ]
    run1    = []
    # run2    = []
    # run3    = []
    # run4    = []

    with open(results, 'r') as f:
        for line in f:
            #if the first character is '{' parse it as a dictionary
            if line[0] == '{':
                entry = ast.literal_eval(line)
                #ASSUMPTION: there are 3 runs, one with 2 core nodes,
                #one with 3 core nodes and one with 5. We could make this
                #code more generic to not make this assumption.
                # if entry['nodes'] == 2:
                run1.append(entry['seconds'])
                # if entry['nodes'] == 5:
                #     run2.append(entry['seconds'])
                # if entry['nodes'] == 10:
                #     run3.append(entry['seconds'])
                # if entry['nodes'] == 7:
                #     run4.append(entry['seconds'])


        run1_line = plt.plot(xpoints,run1, "r--", label="2 core nodes, 5 core nodes and 10 core nodes")
        # run2_line = plt.plot(xpoints,run2, "bs-", label="run2 (5 core nodes)")
        # run3_line = plt.plot(xpoints,run3, "g^-", label="run3 (10 core nodes)")
        # run4_line = plt.plot(xpoints,run4, "k--", label="run4 (3 core nodes and 4 task nodes)")

        plt.title("A comparision of three runs")
        plt.legend()

        # This would display locally:
        # plt.show()
        # This will save the figure as a png and as a PDF:
        plt.savefig(png_fname)
        plt.savefig(pdf_fname)


plot("q3_results.txt")



