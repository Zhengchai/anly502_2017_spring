import matplotlib.pyplot as plt
import numpy as np


x=[]
y=[]
with open('q5_result.txt') as f:
    for line in f:
        word =line.split()
        x.append(word[1][1:-1])
        y.append(word[0])
inty = [int(key) for key in y]

ind =np.arange(10)
width = 0.8
plt.bar(ind,inty,width,color='blue')
plt.ylabel('Number')
plt.title('Bar graph of the top-10 hits')
j=770000
for i in x:
    plt.text(3,j,i)
    j=j-30000

k=770000
for i in y:
    plt.text(1.5,k,i)
    k=k-30000

# plt.xticks(ind, x)
plt.show()
plt.savefig("q5_plot.pdf")
plt.savefig("q5_plot.png")