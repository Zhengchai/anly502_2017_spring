#!/usr/bin/python3.5
#
# generate the streaming time report


if __name__=="__main__":
    import argparse
    import os
    import datetime
    import commands

    parser = argparse.ArgumentParser()
    parser.add_argument("s3",help="S3 URL")
    parser.add_argument("output",help="Output filename")
    
    args = parser.parse_args()

    start_time=datetime.datetime.now()

    instance_type=commands.getoutput('ec2-metadata -t')
    instance_type=instance_type.lstrip('instance-type:')
    instance_type=instance_type.lstrip(' ')

    cmd = ['aws s3 cp',args.s3, '- | grep  "fnard:-1 fnok:-1 cark:-1 gnuck:-1" | wc -l']
    str=" ".join(cmd)
    print (str)

    malfunctions=commands.getoutput(str)

    end_time=datetime.datetime.now()
    run_time=(end_time-start_time).seconds
    start_time=start_time+ datetime.timedelta(hours=-5)
    iso_time=start_time.isoformat()

    if os.path.exists(args.output):
        raise RuntimeError("{} exists".format(args.output))
    
    # Create the output file
    with open(args.output,"w") as f:
        f.write("# streaming to a {}\n".format(instance_type))
        f.write("Date: {}\n".format(iso_time))
        f.write("mode: streaming\n")
        f.write("streaming: {}\n".format(run_time))
        f.write("malfunctions: {}\n".format(malfunctions))
