#!/usr/bin/env python35
"""
================
CDF Date Plot
================

Based on http://matplotlib.org/examples/api/date_demo.html

All matplotlib date plotting is done by converting date instances into
days since the 0001-01-01 UTC.  The conversion, tick locating and
formatting is done behind the scenes so this is most transparent to
you.  The dates module provides several converter functions date2num
and num2date

References:

https://docs.scipy.org/doc/numpy/user/basics.creation.html
http://stackoverflow.com/questions/9378420/how-to-plot-cdf-in-matplotlib-in-python
http://stackoverflow.com/questions/969285/how-do-i-translate-a-iso-8601-datetime-string-into-a-python-datetime-object
https://docs.scipy.org/doc/numpy/reference/generated/numpy.loadtxt.html
https://docs.scipy.org/doc/numpy/user/basics.io.genfromtxt.html

"""

def timestamp_cdf(infiles,outfile):
    import json
    import datetime
    import numpy as np
    import matplotlib.pyplot as plt
    import matplotlib.dates as mdates
    data = []

    # First create an array with all of the JSON objects...
    for infile in infiles:
        for line in open(infile,"rU"):
            # only process lines beginning with JSON:
            if line[0]=='{':
                try:
                    data.append(json.loads(line))
                except json.decoder.JSONDecodeError as e:
                    print("  Invalid JSON in {}".format(infile))
        
    # Extract the dates and convert to an np.array
    dates_list = [ datetime.datetime.fromtimestamp( d['date']) for d in data]
    
    # convert to an np.array:
    dates = np.array( sorted( dates_list ) )

    # Create a vector of 1s that's the same length
    submissions = np.array( [1] * len(dates) )

    fig, ax = plt.subplots()
    ax.plot(dates,np.cumsum(submissions)) # make cumulative 

    # format the ticks
    ax.xaxis.set_major_locator(mdates.DayLocator())
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M'))
    #ax.xaxis.set_minor_locator(months)

    # Find earliest time on the datetime and nextday
    def truncated_datetime(x):
        return datetime.date(x.year, x.month, x.day)

    earliest_day = truncated_datetime(dates.min())
    next_day     = truncated_datetime(dates.max()+datetime.timedelta(days=1))
    ax.set_xlim(earliest_day,next_day)


    # format the coords message box
    ax.format_xdata = mdates.DateFormatter('%Y-%m-%d')
    #ax.format_ydata = lambda x:'%i DAY' % x

    # rotates and right aligns the x labels, and moves the bottom of the
    # axes up to make room for them
    fig.autofmt_xdate()
    
    # Save it!
    if outfile:
        plt.savefig(outfile)
    else:
        plt.show()


if __name__=="__main__":
    import sys,os,argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("infiles",nargs="+",help="files to graph")
    parser.add_argument("--outfile",help="output for graph")
    args = parser.parse_args()
    timestamp_cdf(args.infiles,args.outfile)

