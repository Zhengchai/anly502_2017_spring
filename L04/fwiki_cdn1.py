#!/usr/bin/env python34
# 
# Analyze forensics wiki input file
# For information on MRJOB see:
# https://pythonhosted.org/mrjob/guides/quickstart.html

from mrjob.job import MRJob
import re

CLR_RE = re.compile(r'^(\S+) (\S+) (\S+) \[([^\]]+)\] "(\S+) (\S+) \S+" (\S+) (\S+) "([^"]*)" "([^"]*)"')

class FWikiAnalyzer(MRJob):

    PARTITIONER = "org.apache.hadoop.mapred.lib.HashPartitioner"

    def mapper(self, _, line):
        m = CLR_RE.search(line)
        try:
            if m:
                yield ( m.group(6), int( m.group(8) ) ) , 1
            else:
                self.increment_counter("Info","CLR_RE Unmatched Lines",1)
        except RuntimeError as e:
            self.increment_counter("Error","RuntimeError in FWikiAnalyzer.mapper",1)
            pass

    def reducer(self, url_count, ones):
        yield url_count, sum(ones)


if __name__ == '__main__':
    FWikiAnalyzer.run()

