#!/usr/bin/env python34

import pytest
from fwiki_cdn1 import *

# Sample weblog entry
log='77.21.0.59 - - [01/Jan/2012:00:35:04 -0800] "GET /w/skins/common/wikibits.js?270 HTTP/1.1" 200 31165 "http://www.forensicswiki.org/wiki/Write_Blockers" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/534.52.7 (KHTML, like Gecko) Version/5.1.2 Safari/534.52.7"'


# create a test
def test_CLR_RE():
    m = CLR_RE.search(log)
    assert m.group(1)=="77.21.0.59"
    assert m.group(2)=="-"
    assert m.group(3)=="-"
    assert m.group(4)=="01/Jan/2012:00:35:04 -0800"
    assert m.group(5)=="GET"
    assert m.group(6)=="/w/skins/common/wikibits.js?270"
    assert m.group(7)=="200"
    assert m.group(8)=="31165"
    assert m.group(9)=="http://www.forensicswiki.org/wiki/Write_Blockers"
    assert m.group(10)=="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/534.52.7 (KHTML, like Gecko) Version/5.1.2 Safari/534.52.7"

def test_LogLine():
    import datetime
    w = LogLine(log)
    assert w.ipaddr=='77.21.0.59'
    assert w.datetime==datetime.datetime(2012, 1, 1, 0, 35, 4, tzinfo=datetime.timezone(datetime.timedelta(-1, 57600)))
    assert w.verb=='GET'
    assert w.path=='/w/skins/common/wikibits.js?270'
    assert w.code==200
    assert w.bytes==31165
    assert w.user_agent=='Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/534.52.7 (KHTML, like Gecko) Version/5.1.2 Safari/534.52.7'

def test_LogLine_slots():
    # Make sure slots are working
    w = LogLine(log)
    try:
        w.foo = 'bar'
        assert False            # Previous line should generate attribute error
    except AttributeError as e:
        pass                    # we are supposed to get an attribute error!

def test_mapper():
    for (key, value) in FWikiAnalyzer.mapper(None,"",log):
        assert key[0] == "/w/skins/common/wikibits.js?270"
        assert key[1] == 31165
        assert value  == 1


def test_reducer():
    key = ("/w/skins/common/wikibits.js?270",31165)
    values = [1,1,1,1]
    for (key2, value2) in FWikiAnalyzer.reducer(None, key, values):
        assert key2[0] == "/w/skins/common/wikibits.js?270"
        assert key2[1] == 31165
        assert value2  == 4

