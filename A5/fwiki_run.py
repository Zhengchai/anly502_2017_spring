#!/usr/bin/env python3
#
# Run this program with spark-submit

import sys,os,datetime,re,operator
from pyspark import SparkContext, SparkConf
from fwiki import LogLine



if __name__=="__main__":
    # Get your sparkcontext and make a dataframe
    from pyspark.sql import SparkSession
    spark = SparkSession.builder.appName("fwiki").getOrCreate()
    sc    = spark.sparkContext      # get the context
    
    # Create an RDD from s3://gu-anly502/logs/forensicswiki.2012.txt
    url = "s3://gu-anly502/logs/forensicswiki.2012.txt"
    # url = "/Users/chenyujing/Desktop/forensicswiki.2012.txt"


    # NOTE: Do this with 1 master m3.xlarge, 2 core m3.xlarge, and 4 task m3.xlarge
    # otherwise it will take forever...
    
    # loglines = <<<insert code to read the logfiles into a RDD >>>
    # logs     = <<<insert code to convert the loglines into an RDD of Row() records >>>
    # df       = <<<insert code to convert logs into a DataFrame using spark.createDataFrame() >>>
    loglines = sc.textFile(url)
    logs = loglines.map(lambda l: LogLine(l).row())
    df = spark.createDataFrame(logs).cache()

    
    # Register the dataframe as an SQL table called 'logs'
    df.createOrReplaceTempView("logs")
    result = open("fwiki_run.txt","w")
    # Print how many log lines there are
    print("Total Log Lines: {}".format(spark.sql("select count(*) from logs").collect()))
    result.write("Total Log Lines: "+str(spark.sql("select count(*) from logs").collect()[0][0])+'\n')
    # Figure out when it started and ended
    (start,end) = spark.sql("select min(datetime),max(datetime) from logs").collect()[0]

    print("Date range: {} to {}".format(start,end))
    result.write("Date range: "+str(start)+ " to "+str(end)+'\n')
    

    for month,count in spark.sql("SELECT SUBSTR(datetime,1,7) as month,COUNT(1) from logs GROUP BY 1 order by month").collect():
        print (month, count)
        if month != None:
            result.write(str(month)+'\t' +str(count)+'\n')
    result.close()

    # Now generate the requested output
    # print ("The month count: {}".format(spark.sql("SELECT SUBSTR(datetime,1,7),COUNT(1) from logs GROUP BY 1").collect()))


