import boto3
import json
from urllib.request import urlopen
import collections
from boto3.dynamodb.conditions import Key,Attr

if __name__=="__main__":
    dd = boto3.resource('dynamodb',region_name="us-east-1")
    data = json.loads(urlopen("http://data.consumerfinance.gov/api/views/7zpz-7ury/rows.json").read().decode('utf-8'))

    # The attributes are too many, so I just create the table with two attributes, it's enough to calculate the results
    # dd.Table('zc104').delete()
    table = dd.create_table(
    	TableName = 'zc104',
    	KeySchema = [
    	{
    	    "AttributeName": 'date_year',
    	    "KeyType": 'HASH'
    	},
            {
            "AttributeName": 'sid',
            "KeyType": 'RANGE'
            }
    	],
    	AttributeDefinitions = [
    	{
    	    "AttributeName" : 'date_year',
    	    "AttributeType" : 'S'
    	},
            {
                 "AttributeName" : 'sid',
                 "AttributeType" : 'N'
            }
    	],
    	ProvisionedThroughput={
    	    'ReadCapacityUnits': 500,
    	    'WriteCapacityUnits': 500,
    	}
    	
    )
    table.wait_until_exists()
    # Get the no duplicate year list and put data into dynamodb
    year_list = []
    for i in data['data']:
        if i[8][0:4] not in year_list:
            year_list.append(i[8][0:4])
        table.put_item(Item ={'date_year': i[8][0:4],'sid' : i[0],})
    # Sort the year list
    year_list = sorted(year_list)
    #  Query the number of each year in year_list
    with open("complaints.txt","w") as f :
        for i in year_list:
            count = table.query(Select = 'COUNT',KeyConditionExpression = Key('date_year').eq(i))
            print (str(i) +'\t'+str(count['Count']))
            f.write(str(i) +'\t'+str(count['Count'])+'\n')
