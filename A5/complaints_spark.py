#!/usr/bin/env python3
#

import sys,os,datetime,re,operator
from pyspark import SparkContext, SparkConf
from pyspark.sql import SparkSession
import json
from urllib.request import urlopen
from pyspark.sql import Row 

# def getcolumns():
#     columns = []
#     for i in data['meta']['view']['columns']:
#         columns.append(i['name'])
#     return columns

if __name__=="__main__":
    
    spark = SparkSession.builder.appName("complaints_spark").getOrCreate()
    sc    = spark.sparkContext     
    
    data = json.loads(urlopen("http://data.consumerfinance.gov/api/views/7zpz-7ury/rows.json").read().decode('utf-8'))

    data_rdd =  sc.parallelize(data['data'])

    df = spark.createDataFrame(data_rdd.map(lambda q: Row(year=q[8])))

    
    # # Register the dataframe as an SQL table called 'complaints_spark'
    df.createOrReplaceTempView("complaints_spark")

    result = spark.sql("select substring(year,1,4), count(*) from complaints_spark group by substring(year,1,4) order by substring(year,1,4)").collect()

    with open('complaints.txt','w') as f:
        for i in result:
            f.write(str(i[0])+'\t'+str(i[1])+'\n')
