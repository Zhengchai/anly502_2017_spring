import json
from urllib.request import urlopen
import collections

data = json.loads(urlopen("http://data.consumerfinance.gov/api/views/7zpz-7ury/rows.json").read().decode('utf-8'))
# Compute the number of compaints for each year
year_count_dict = dict()
for i in data['data']:
    year = i[8][0:4]
    if year in year_count_dict.keys():
        year_count_dict[year] += 1
    else:
        year_count_dict[year] = 1

year_count_dict = collections.OrderedDict(sorted(year_count_dict.items()))

with open('complaints.txt','w') as f:
    for key in year_count_dict.keys():
        f.write(str(key)+'\t'+str(year_count_dict[key])+'\n')

# How many complaints were there against PayPal Holdings, Inc
paypal_count = 0
for i in data['data']:
    if i[15] == 'PayPal Holdings, Inc.':
        paypal_count +=1
with open('paypal.txt','w') as f:
    f.write(str(paypal_count)+'\n')
