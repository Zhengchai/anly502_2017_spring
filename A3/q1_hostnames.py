#!/usr/bin/python3.4
#
# Template to compute the number of distinct IP addresses 
#

from mrjob.job import MRJob
import re
import mrjob

HOST_RE = re.compile(r"((\w+)+(\-\w+){1,10}(\.\w+){1,10})")

class DistinctHOST(MRJob):
    OUTPUT_PROTOCOL = mrjob.protocol.TextProtocol

    def mapper(self, _, line):
    	for host in HOST_RE.findall(line):
    		word = host[0]
    		yield word, 1

        # Your code goes here

    def reducer(self, word, counts):
    	yield word, str(sum(counts))

        # Your code goes here


if __name__ == '__main__':
    DistinctHOST.run()
