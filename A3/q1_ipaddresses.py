#!/usr/bin/python3.4
#
# Template to compute the number of distinct IP addresses 
#

from mrjob.job import MRJob
import re
import mrjob

IP_RE = re.compile(r"((?:(?:25[0-5]|2[0-4]\d|((1\d{2})|([1-9]?\d)))\.){3}(?:25[0-5]|2[0-4]\d|((1\d{2})|([1-9]?\d))))")

class DistinctIPAddresses(MRJob):
    OUTPUT_PROTOCOL = mrjob.protocol.TextProtocol
    def mapper(self, _, line):
    	for ip in IP_RE.findall(line):
    		word = ip[0]
    		yield word, 1

        # Your code goes here

    def reducer(self, word, counts):
    	yield word,str(sum(counts))

        # Your code goes here


if __name__ == '__main__':
    DistinctIPAddresses.run()
