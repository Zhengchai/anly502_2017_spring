#!/usr/bin/python3.4
#
# Template to compute the number of distinct IP addresses 
#

from mrjob.job import MRJob
import re
import mrjob

HostAndIp_RE= re.compile(r"((\w+)+(\-\w+){1,10}(\.\w+){1,10}\,(?:(?:25[0-5]|2[0-4]\d|((1\d{2})|([1-9]?\d)))\.){3}(?:25[0-5]|2[0-4]\d|((1\d{2})|([1-9]?\d))))")

class HostAndIP(MRJob):
    OUTPUT_PROTOCOL = mrjob.protocol.TextProtocol

    def mapper(self, _, line):
    	for HostAndIp in HostAndIp_RE.findall(line):
    		temp =HostAndIp[0]
    		ipAdd = temp.split(",")[1]
    		hostName = temp.split(",")[0]
    		yield ipAdd, hostName

        # Your code goes here

    def reducer(self, ip, host):
    	final= []
    	for host in host:
    		if host not in final:
    			final.append(host)
    	if len(final)>1:
    		final=','.join(final)
    		yield ip, final


        # Your code goes here


if __name__ == '__main__':
    HostAndIP.run()
