#!/usr/bin/python3.4
#
# Template to compute the number of distinct IP addresses 
#

from mrjob.job import MRJob
import re
import statistics
import mrjob



RE = re.compile(r"((?:(?:25[0-5]|2[0-4]\d|((1\d{2})|([1-9]?\d)))\.){3}(?:25[0-5]|2[0-4]\d|((1\d{2})|([1-9]?\d)))\,([0-9]*))")

class Links(MRJob):
    OUTPUT_PROTOCOL = mrjob.protocol.TextProtocol

    def mapper(self, _, line):
        temp_ip=[]
        temp_microseconds=[]
        for Str in RE.findall(line):
            temp = Str[0] 
            ipAdd = temp.split(",")[0]
            microseconds = int(temp.split(",")[1])
            temp_ip.append(ipAdd)
            temp_microseconds.append(microseconds)
        if len(temp_ip)>1 and len(temp_microseconds)==len(temp_ip):
            for index in range(len(temp_ip)-1):
                yield (temp_ip[index], temp_ip[index+1]), (temp_microseconds[index+1]-temp_microseconds[index])
                # yield (temp_ip[index], temp_ip[index+1]), 1



    def reducer(self, ipStr,microSec):
        ip1 = ipStr[0]
        ip2 = ipStr[1]
        microSecList=[]
        microSecList.extend(microSec)
        # if len(microSecList)>0:
        #     yield (ip1 +"->" +ip2), str(len(microSecList))
        # else:
        #     raise RuntimeError("Error")

        if len(microSecList)>1:
            yield (ip1 +"->" +ip2), str(len(microSecList))+'\t'+str(statistics.pstdev(microSecList))
        elif len(microSecList)==1:
            yield (ip1 +"->" +ip2), str(1)+'\t'+str(0)
        else:
            raise RuntimeError("Error")





if __name__ == '__main__':
    Links.run()
