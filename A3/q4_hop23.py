#!/usr/bin/python3.4
#
# Template to compute the number of distinct IP addresses 
#

from mrjob.job import MRJob
import re
import statistics
import mrjob



RE = re.compile(r"((?:(?:25[0-5]|2[0-4]\d|((1\d{2})|([1-9]?\d)))\.){3}(?:25[0-5]|2[0-4]\d|((1\d{2})|([1-9]?\d)))\,([0-9]*))")
Time_RE = re.compile(r"^\d{4}\-\d{2}\-\d{2}T\d{2}")

class Hope23(MRJob):
    OUTPUT_PROTOCOL = mrjob.protocol.TextProtocol

    def mapper(self, _, line):
        temp_microseconds=[]
        temp_time = []
        for Str in RE.findall(line):
            temp = Str[0] 
            microseconds = int(temp.split(",")[1])
            temp_microseconds.append(microseconds)
        for time in Time_RE.findall(line):

            temp_time.append(time[-2:])

        if len(temp_microseconds)>2:
            yield temp_time, (temp_microseconds[2]-temp_microseconds[1])



    def reducer(self, time,microSec):
        for iter in time:
            yield iter, str(statistics.pstdev(microSec))
  

if __name__ == '__main__':
    Hope23.run()
