import matplotlib.pyplot as plt
import numpy as np


x=[]
y=[]
with open('q4_hop23.txt') as f:
    for line in f:
        word =line.split("\t")
        x.append(word[0])
        y.append(word[1])

plt.title('Line graph of Stddev in each hour')
plt.xlabel("Hour")
plt.ylabel("Stddev")
plt.grid(True)
plt.xlim(00,23)
plt.xticks(np.linspace(00,23,24,endpoint=True))

plt.plot(x,y,color="red",linewidth=2)
plt.savefig('q4_hop23.png')
plt.savefig('q4_hop23.pdf')

plt.show()
plt.close()